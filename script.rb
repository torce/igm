#!/usr/bin/env ruby
require 'fileutils'

acc = {
    bvh: {
      'maxnodeprims' => [4],
      'splitmethod' => ['sah']
    },
    kdtree: {
      'intersectcost' => [80],
      'traversalcost' => [1],
      'emptybonus' => [0.5],
      'maxprims' => [1],
      'maxdepth' => [-1]
    },
    grid: {
      'refineimmediately' => [false]
    }
  }

#Directorio donde se guardan las escenas
scene_dir = '/home/david/projects/pbrt-v2/scenes/'
#Ruta al ejecutable de pbrt
pbrt_command = '/home/david/projects/pbrt-v2/src/bin/pbrt'

syrupy_command = './syrupy.py  -C --separator ","'
#Directorio de salida
out_dir = '/home/david/projects/pbrt-v2/out/'

scenes = %w(bunny killeroo-simple metal)

clean_output_patterns = [/Rendering\:.*/, /Direct Lighting\:.*/, /Metropolis\:.*/]

def write_parameter(name, param)
    if param.is_a? Integer
      "\"integer #{name}\" #{param} "
    elsif param.is_a? String
      "\"string #{name}\" \"#{param}\" "
    elsif param.is_a? Float
      "\"float #{name}\" #{param} "
    elsif !!param == param
      "\"bool #{name}\" \"#{param}\" "
    end
end

names = scenes.flat_map do |scene|
  created_files = []
  text = File.read(scene_dir + scene + '.pbrt')
  acc.map do |acc, attrs|
    t = attrs.values[0]
    (0..(attrs.values[0].size-1)).each do |i|
      command = "Accelerator \"#{acc}\" "
      attrs.each do |attr, values|
        command << write_parameter(attr, values[i])
      end
      result = "#{command}\n#{text.gsub(/^Accelerator\s.+/, '')}"
      name = "#{scene}-#{acc}-#{command.gsub(/\s/,'-').gsub(/\"/,'')}"
      File.open("#{scene_dir + name}.pbrt", 'w') { |file| file.puts result }
      created_files << name
    end
  end
  created_files
end

names.each do |name|
  if not File.exists?("#{out_dir + name}.txt")
    puts "Rendering #{name}"
    out = `#{syrupy_command} -t #{out_dir + name} #{pbrt_command} #{scene_dir + name + '.pbrt'} --outfile #{name + '.exr'}`
    puts 'Cleaning output'
    clean_output_patterns.each {|repl| out.gsub!(repl, '')}

    File.open("#{out_dir + name}.txt", 'w') { |file| file.puts out }
  else
    puts "#{name} already exists."
  end
end
